import cv2
import numpy as np

def alinear(imagen_base, imagen_alinear):
        
    # Convertir imagenes a escala de grises
    gray_base = cv2.cvtColor(imagen_base,cv2.COLOR_RGB2GRAY)
    grat_alinear = cv2.cvtColor(imagen_alinear,cv2.COLOR_RGB2GRAY)

    # Tamaño imagen base
    tamaño_base = imagen_base.shape

    # Definir criterios para la alineacion y demas parametros
            # cv2.TERM_CRITERIA_EPS termina si cumple con el error
            # cv2.TERM_CRITERIA_COUNT termina si cumple con las iteraciones
            # Por defecto findTransformECC (iteraciones = 50, error = 0.001)

    iteraciones = 5000;
    error = 1e-10;
    
    # Crear una matriz de identidad 2x3
    matriz_identidad = np.eye(2, 3, dtype=np.float32)

    # Definimos criterio para definir el momento de finalizacion en la interacion de ECC transform
    # Criterios: valor esperado de error y cantidad maxima de iteraciones
    criterio = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, iteraciones, error)
    
    try:
        # Se corre el algoritmo ECC. Los resultados son guardados en matriz_redef, que servira luego para alinear la imagen
        # deseada. Luego, coef_corr guarda coeficientes de correlacion, pero no son usados, si bien son necesarios para la
        # transdormada

        (coef_corr, matriz_redef) = cv2.findTransformECC (gray_base, grat_alinear, matriz_identidad, cv2.MOTION_TRANSLATION, criterio)

        # warpAffine  utiliza la matriz redefinida para crear una nueva imagen alineada
        imagen_alineada = cv2.warpAffine(imagen_alinear, matriz_redef, (tamaño_base[1],tamaño_base[0]), flags=cv2.INTER_LINEAR + cv2.WARP_INVERSE_MAP)
    
        return imagen_alineada 
    except:
        return imagen_base

    