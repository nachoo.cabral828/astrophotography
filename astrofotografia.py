# Librerias utilizadas
import sys
import cv2
import numpy as np
import os
import alinear
import focus_stack
import preprocess

### Ayuda ###
# Sintaxis:
# ./astrofotografia.py directorio nombre_salida.jpg 
### Ayuda ###

# 1) Leer entradas
directorio = sys.argv[1]
nombre_salida = sys.argv[2]

# 2) Abrir imagenes (Formato Raw)
lista_imagenes = os.listdir(directorio)
print ("Abriendo imagenes desde: ", directorio)
print("Cantidad de imagenes: ", len(lista_imagenes))

## 2.a) Algoritmo para abrir imagenes
imagenes = []
for i in lista_imagenes:
    if (i == lista_imagenes[1]):
        imagen = preprocess.abrir_imagen_raw(i,directorio)
        # Si se desea ver solo el efecto del filtro, guardar una imagen modificada, descomentando la siguiente linea:
        cv2.imwrite('./MejoradaBase.jpg',imagen)
    else:
        imagen = preprocess.abrir_imagen_raw(i,directorio)
    imagenes.append(imagen)

# 3) Algoritmo de Focus Staking
## 3.a) Alineado
l = len(imagenes)
imagenes_alineadas = []
contador = 0

print('Alineando imagen:  1')
for i in range(1,l):
        print('Alineando imagen: ', i+1)
        resultado = alinear.alinear(imagenes[0], imagenes[i])
        if((resultado != imagenes[0]).all):
            contador += 1
            imagenes_alineadas.append(resultado)
print('Se pudieron alinear ',contador,' imagenes')

## 3.b) Focus Staking 
print('Apilando imagenes alineadas')
imagen_enfocada = focus_stack.focus_stack(imagenes_alineadas)

# 4) Guardado
cv2.imwrite(nombre_salida,imagen_enfocada)
 
