# Astrophotography

Este repositorio contiene Scripts realizados en Python que buscan automatizar el postprocesamiento de fotografías lunares.

## Resumen

Dicho proyecto consiste en un codigo que abre un directorio de imagenes en formato raw, las procesa aplicandole unos retoques de blancos, negros y brillo. Posteriormente se le aplica un histograma stretching mas un ajuste adicional de histograma. Luego pasa a una etapa de alineacion y termina con un apilamiento de enfoque.
Se obtiene como resultado una imagen en jpg mejorada de las originales.

## Antes de comenzar a utilizar clone el repositorio

git clone https://gitlab.com/nachoo.cabral828/astrophotography.git

## Librerias necesarias

Son necesarias: 
- numpy: pip install numpy
- opencv: pip install opencv-python
- rawpy: pip install rawpy

## Ejecucion

python astrofotografia.py "directorio" "nombre de salida"

Ejemplo:
python astrofotografia.py ".\base_datos\fotos_ejemplo" ".\prueba1.jpg"

## Recomendaciones

- Se recominda pre-seleccionar las imagenes dejando las que tengan mejor enfoque para obtener mejores resultados.
- Utilizar el archivo prueba.py para calibrar la lectura de las imagenes. De esta manera se asegura un resultado optimo luego de procesar.
- En conjunto a prueba.py ir modificando el archivo preprocess.py donde se debe configurar previamente el balance de negros, brillo, y blancos.

## Autores

- Ignacio Cabral
- Matias Caminati
- Franco Del Sole
