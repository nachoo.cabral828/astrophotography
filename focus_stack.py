import cv2
import numpy as np

def focus_stack(imagenes_alineadas):
    # Entran a la funcion las imagenes ya alineadas, si no se pasa con la alineacion se vera el efecto
    # de corrimiento

    group = []
    for i in range(len(imagenes_alineadas)):
        # Trabajar en grises para optimizar el algoritmo
        imagen_gray = cv2.cvtColor(imagenes_alineadas[i],cv2.COLOR_BGR2GRAY)
        # Aplicar suavizado para evitar deformacion de bordes por calidad de imagen 
        # Si esta borrosa, este filtro ayudara a facilitar las estimaciones posteriores
        imagen_suavizada = cv2.GaussianBlur(imagen_gray, (5,5), 0)

        #  Aplicar laplaciano para encontrar puntos de mayor enfoque en cada imagen
        #  Esto se obtiene mediente la derivada de segundo orden detectando frecuencias = enfoques
        imagen_focus = cv2.Laplacian(imagen_suavizada, cv2.CV_64F, ksize=5)
    
        group.append(imagen_focus)
   
    # Tranformacion a un array
    group = np.asarray(group)
    
    # Imagen de salida vacia
    imagen_enfocada = np.zeros(shape=imagenes_alineadas[0].shape, dtype=imagenes_alineadas[0].dtype)
    
    for i in range(0, imagenes_alineadas[0].shape[0]):
        for j in range(0, imagenes_alineadas[0].shape[1]):
            # Se apilan las imagenes y recorriendo cada fila-columna de la imagen se filtran los valores maximos
            # estos valores seran los puntos mas enfocados, son aquellos que se requieren para realizar el
            # focus staking.
            apilamiento = abs(group[:, i, j])

            # where buscara el valor maximo de todas las imagenes en el pixel i,j, maximo enfoque
            posicion = (np.where(apilamiento == max(apilamiento)))[0][0]
            
            imagen_enfocada[i,j] = imagenes_alineadas[posicion][i,j]

    return imagen_enfocada