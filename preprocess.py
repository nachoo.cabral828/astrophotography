import cv2
import numpy as np
import rawpy



def abrir_imagen_raw(imagen,directorio):
    ## Esta funcion abre imagenes de un directorio  y las mejora 
    ## para un proximo procesamiento.
     
    # Abrir imagen con rawpy
	imagen = rawpy.imread(directorio+"/"+imagen)
 
	# Transformaciona a array considerando ajuste de blancos y brillo
    # Este ajuste es muy importante
	# Manual - user_wb=(1,1,1,1) - Ajustar paramentros
	# Automatico - use_auto_wb=True
    # Ajuste de negro si hay ruido atmosferico - user_black = 100 
	imagen = imagen.postprocess(user_black = 13,use_auto_wb=True,bright=0.003)
 
    # Equalizaciones
	imagen = histeq(imagen)	
	imagen = hist_s(imagen)
 
	# Necesario : Float 32 para cvtColor 
	imagen = imagen.astype(np.float32)

	return imagen
	
def hist_s(imagen):
    ## Esta funcion realiza un estiramiento de histograma, es decir,
    ## amplia el contraste de la imagen.
    
	# Nacesario: imagen =  array	
	constante = (255-0)/(imagen.max()-imagen.min())
	img_stretch = imagen * constante

	return img_stretch

def histeq(im):
    ## Esta funcion realiza una equializacion localizada
    
    # Recomendado: pasar a escalas de grices 
    im = cv2.cvtColor(im,cv2.COLOR_RGB2GRAY)
    
    # Funcion = cv2.createCLAHE - 
    # - clipLimit diferencia de contraste
    # - tileGridSize cuadrado de pixeles a tomar para equalizar
    clahe = cv2.createCLAHE(clipLimit=3.0, tileGridSize=(6,6))
    im_clahe = clahe.apply(im)
    
    # Regresar a RGB
    im_clahe = cv2.cvtColor(im_clahe,cv2.COLOR_GRAY2RGB)
    
    return im_clahe